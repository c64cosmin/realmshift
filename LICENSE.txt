RealmShift mod - Copyright (c) 2017 c64cosmin@gmail.com

Distribution of this software, in this form, other form (e.g. JAR),
or modified version, is not endorsed by the author, unless credit is offered
in the form of the above copyright notice.
Using, executing or storing this software for either client or server usage,
is hereby granted, free of charge.
Copying or reusing original work from any of the software's components
(code or assets) without author's consent, in other projects is prohibited.
Using the source code of the software as a didactical reference is allowed.
Contributing work in any form is allowed, only if credit will be appended
by the author in the COPYRIGHT.txt file.

The license applies only to the software (executable, code or assets) that
can be found on [Bitbucket repository](https://bitbucket.org/stupidrat/realmshift)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[Minecraft Forge homepage](https://github.com/MinecraftForge/FML)

"Minecraft" is a trademark of Mojang Synergies AB. Mojang © 2009-2017. 
