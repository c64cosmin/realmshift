package com.stupidrat.minecraft.realmshift.tileentities;

import com.stupidrat.minecraft.realmshift.RealmShiftUtil;
import com.stupidrat.minecraft.realmshift.blocks.RealmIgniter;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

public class RealmIgniterEntity extends BlockEntity implements Tickable{
    private class Vec3f{
        public float x,y,z;
        public Vec3f(float x, float y, float z){
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
    private class Vec2f{
        public float x,y,z;
        public Vec2f(float x, float y){
            this.x = x;
            this.y = y;
        }
    }
    private int tunnelLength;
    private Vec3f[] tunnelPoints;
    private Vec3f[] tunnelParams;
    private Vec3f[] tunnelX;
    private Vec3f[] tunnelY;
    private Vec3f[] vecs;
    private Vec2f[] uvs;
    public boolean hasPortal;
    public float counter;
    public boolean initSound = true;
    public RealmIgniterEntity(){
        super(RealmShiftBlockEntities.REALM_IGNITER_PORTAL);
        
        hasPortal = false;
    }
    @Override
    public void tick() {
        if(counter%10==0){
            findPortal();
        }
        counter++;
        if(counter>100000)counter-=100000;
        if(this.world.isClient){
            buildTunnel();
            updateTunnel();

            if(hasPortal &&
               this.world.random.nextDouble() < 0.09){
                //this.world.playSound(pos.getX(), pos.getY(), pos.getZ(), SoundEvents.BLOCK_FURNACE_FIRE_CRACKLE, SoundCategory.BLOCKS, 0.2f, 0.1f+world.rand.nextFloat(), false);
                for(int i=0;i<3;i++)
                    world.addParticle(ParticleTypes.SMOKE, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, world.random.nextDouble()*0.2-0.1, world.random.nextDouble()*0.2-0.1, world.random.nextDouble()*0.2-0.1);
            }
            if(isActive()){
                if(initSound) {
                    counter = 0;
                    initSound = false;
                }
                if(counter % (32 * 20) == 0)
                {
                    /*TODO
                    portalSound = SoundHelper.playSound(pos.getX(), pos.getY(), pos.getZ(), RealmShiftSounds.REALM_IGNITER_PORTAL_AMBIENT, SoundCategory.BLOCKS, 4.0f, 1.0f, false);
                    */
                }
                world.addParticle(ParticleTypes.FLAME, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, world.random.nextDouble()*0.2-0.1, world.random.nextDouble()*0.2-0.1, world.random.nextDouble()*0.2-0.1);
            }
        }
        else{
            if(!hasPortal && isActive()){
                /*TODO
                ((RealmIgniter)RealmShiftBlocks.REALM_IGNITER).deactivatePortal(this.world, this, getPos(), this.world.getBlockState(getPos()));
                */
            }
        }
    }
    
    //private SimpleSound portalSound;
    
    public void stopSounds() {
        //MinecraftClient.getInstance().getSoundHandler().stop(portalSound);
    }

    BlockPos portalRoot;
    Direction portalFacing;
    int portalWidth, portalHeight;

    BlockPos oldPortalRoot;
    Direction oldPortalFacing;
    int oldPortalWidth, oldPortalHeight;
    private boolean didInit;
    private void findPortal(){
        if(portalRoot != null)
            oldPortalRoot = new BlockPos(portalRoot);
        oldPortalFacing = portalFacing;
        oldPortalWidth = portalWidth;
        oldPortalHeight = portalHeight;

        hasPortal = false;
        for(int x=-15;x<=15;x++)
        for(int y=-15;y<=15;y++)
        if(isPortalBlockAt(pos.add(x, 0, y))){
            boolean wasPortal = testForPortal(pos.add(x,0,y));
            if(wasPortal){
                hasPortal = true;
                break;
            }
        }

    }

    private boolean testForPortal(BlockPos position) {
        while(isPortalBlockAt(position.down())){
            position = position.down();
        }
        while(isPortalBlockAt(position.north())){
            position = position.north();
        }
        while(isPortalBlockAt(position.west())){
            position = position.west();
        }
        boolean isPortal = true;
        BlockPos iterator;
        int height = 0;
        int width = 0;
        iterator = new BlockPos(position);
        //determine the height
        while(isPortalBlockAt(iterator)){
            height++;
            if(height>30)return false;
            iterator = iterator.up();
        }
        iterator = new BlockPos(position);
        //determine the facing direction of the portal with respect with the Igniter
        portalFacing = null;
        if(isPortalBlockAt(iterator.south())){
            if(pos.getX()<iterator.getX()){
                portalFacing = Direction.EAST;
            }
            else{
                portalFacing = Direction.WEST;
            }
        }
        else if(isPortalBlockAt(iterator.east())){
            if(pos.getZ()<iterator.getZ()){
                portalFacing = Direction.SOUTH;
            }
            else{
                portalFacing = Direction.NORTH;
            }
        }
        if(this.world.getBlockState(pos).getBlock() == RealmShiftBlocks.REALM_IGNITER)
        if(this.world.getBlockState(pos).get(RealmIgniter.FACING).getOpposite() != portalFacing)return false;
        if(portalFacing == null)return false;
        //determine the width
        while(isPortalBlockAt(iterator)){
            width++;
            if(width>30)return false;
            if(portalFacing == Direction.WEST ||
               portalFacing == Direction.EAST){
                iterator = iterator.south();
            }
            else if(portalFacing == Direction.NORTH ||
                    portalFacing == Direction.SOUTH){
                iterator = iterator.east();
            }
        }
        //test if proper portal
        boolean properPortal = true;
        //opposite corner
        iterator = position.add(0, height-1, 0);
        if(portalFacing == Direction.WEST ||
           portalFacing == Direction.EAST){
            iterator = iterator.add(0, 0, width-1);
        }
        else if(portalFacing == Direction.NORTH ||
                portalFacing == Direction.SOUTH){
            iterator = iterator.add(width-1, 0, 0);
        }
        //validate assumption
        BlockPos iterator2;
        iterator2 = new BlockPos(iterator);
        for(int i=0;i<height;i++){
            if(!isPortalBlockAt(iterator2)){
                properPortal = false;
            }
            iterator2 = iterator2.down();
        }
        iterator2 = new BlockPos(iterator);
        for(int i=0;i<width;i++){
            if(!isPortalBlockAt(iterator2)){
                properPortal = false;
            }
            if(portalFacing == Direction.WEST ||
               portalFacing == Direction.EAST){
                iterator2 = iterator2.north();
            }
            else if(portalFacing == Direction.NORTH ||
                    portalFacing == Direction.SOUTH){
                iterator2 = iterator2.west();
            }
        }

        //verify if the inner part is empty
        for(int x=1;x<width-1;x++)
        for(int y=1;y<height-1;y++){
            int addX = 0;
            int addZ = 0;
            if(portalFacing == Direction.WEST ||
               portalFacing == Direction.EAST){
                addZ = x;
            }
            else if(portalFacing == Direction.NORTH ||
                    portalFacing == Direction.SOUTH){
                addX = x;
            }
            //inside the frame
            Block b;
            b = this.world.getBlockState(position.add(addX, y, addZ)).getBlock();
            if(b != Blocks.AIR&&
               b != RealmShiftBlocks.REALM_IGNITER_PORTAL){
                return false;
            }
            float offsetX = 0;
            float offsetZ = 0;
            if(portalFacing == Direction.WEST){
                offsetX++;
            }
            if(portalFacing == Direction.EAST){
                offsetX--;
            }
            if(portalFacing == Direction.NORTH){
                offsetZ++;
            }
            if(portalFacing == Direction.SOUTH){
                offsetZ--;
            }
            //test for the barrier block zone
            b = this.world.getBlockState(position.add(addX+offsetX, y, addZ+offsetZ)).getBlock();
            if(b != Blocks.AIR&&
               b != Blocks.BARRIER){
                return false;
            }
        }
        portalRoot = new BlockPos(position);
        portalWidth = width;
        portalHeight = height;

        double centerX = portalRoot.getX();
        double centerY = portalRoot.getY()+portalHeight/2f-0.5f;
        double centerZ = portalRoot.getZ();
        if(portalFacing == Direction.WEST ||
           portalFacing == Direction.EAST){
            centerZ += portalWidth/2f-0.5f;
        }
        else if(portalFacing == Direction.NORTH ||
                portalFacing == Direction.SOUTH){
            centerX += portalWidth/2f-0.5f;
        }
        if(pos.getSquaredDistance(centerX, centerY, centerZ, true)<20){
            properPortal = false;
        }
        if(width<4 || height <5){
            properPortal = false;
        }
        if(!properPortal){
            width = height = 0;
        }

        return properPortal;
    }

    private boolean isPortalBlockAt(BlockPos pos) {
        return this.world.getBlockState(pos).getBlock() == RealmShiftBlocks.FORGED_END_STONE;
    }

    public boolean isActive(){
        //TODO fix the stupid bug of respawning :/
        if(this.world.getBlockState(pos).getBlock() != RealmShiftBlocks.REALM_IGNITER)return false;
        return this.world.getBlockState(pos).get(RealmIgniter.ACTIVE);
    }

    private void buildTunnel(){
        if(!hasPortal)return;
        initTunnel();
        float endRotate = 0;
        if(portalFacing == Direction.WEST)endRotate = 90;
        if(portalFacing == Direction.NORTH)endRotate = 180;
        if(portalFacing == Direction.EAST)endRotate = 270;

        tunnelPoints[0].x = 0;
        tunnelPoints[0].y = 0;
        tunnelPoints[0].z = 0;

        tunnelPoints[tunnelLength-1].x = portalRoot.getX()-pos.getX();
        tunnelPoints[tunnelLength-1].y = portalRoot.getY()-pos.getY()+portalHeight/2f-0.5f;
        tunnelPoints[tunnelLength-1].z = portalRoot.getZ()-pos.getZ();

        if(portalFacing == Direction.WEST ||
           portalFacing == Direction.EAST){
            tunnelPoints[tunnelLength-1].z += portalWidth/2f-0.5f;
        }
        else if(portalFacing == Direction.NORTH ||
                portalFacing == Direction.SOUTH){
            tunnelPoints[tunnelLength-1].x += portalWidth/2f-0.5f;
        }

        tunnelParams[0].x = 0.5f;
        tunnelParams[0].y = 0.5f;
        tunnelParams[0].z = endRotate;

        tunnelParams[tunnelLength-1].x = portalWidth-2;
        tunnelParams[tunnelLength-1].y = portalHeight-2;
        tunnelParams[tunnelLength-1].z = endRotate;

        for(int i=1;i<tunnelLength-1;i++){
            float bumpFunction = (float) RealmShiftUtil.bump(1, tunnelLength-2, 1f, i);
            float pxOffset = (float)Math.cos(counter*0.1 +i)*bumpFunction;
            float pyOffset = (float)Math.cos(counter*0.05+i)*bumpFunction;
            float pzOffset = (float)Math.cos(counter*0.2 +i)*bumpFunction;
            tunnelPoints[i].x = (float) (RealmShiftUtil.lerp(tunnelPoints[0].x, tunnelPoints[tunnelLength-1].x, i, tunnelLength) + pxOffset*0.3f);
            tunnelPoints[i].y = (float) (RealmShiftUtil.lerp(tunnelPoints[0].y, tunnelPoints[tunnelLength-1].y, i, tunnelLength) + pyOffset*0.3f);
            tunnelPoints[i].z = (float) (RealmShiftUtil.lerp(tunnelPoints[0].z, tunnelPoints[tunnelLength-1].z, i, tunnelLength) + pzOffset*0.3f);

            tunnelParams[i].x = (float) (RealmShiftUtil.lerp(tunnelParams[0].x, tunnelParams[tunnelLength-1].x, i, tunnelLength) + pxOffset*0.2f);
            tunnelParams[i].y = (float) (RealmShiftUtil.lerp(tunnelParams[0].y, tunnelParams[tunnelLength-1].y, i, tunnelLength) + pxOffset*0.2f);
            tunnelParams[i].z = (float) (RealmShiftUtil.lerp(tunnelParams[0].z, tunnelParams[tunnelLength-1].z, i, tunnelLength) + pyOffset*15f);
        }
    }

    private void initTunnel() {
        if(didInit)return;
        didInit = true;
        counter = 0;

        tunnelLength = 10;
        tunnelPoints = new Vec3f[tunnelLength];
        tunnelParams = new Vec3f[tunnelLength];
        for(int i=0;i<tunnelLength;i++){
            tunnelPoints[i]=new Vec3f(0,0,0);
            tunnelParams[i]=new Vec3f(0,0,0);
        }

        vecs = new Vec3f[4];
        uvs = new Vec2f[4];
        for(int i=0;i<4;i++){
            vecs[i] = new Vec3f(0,0,0);
            uvs[i] = new Vec2f(0,0);
        }

        tunnelX = new Vec3f[tunnelLength];
        tunnelY = new Vec3f[tunnelLength];
        for(int i=0;i<tunnelLength;i++){
            tunnelX[i]=new Vec3f(0,0,0);
            tunnelY[i]=new Vec3f(0,0,0);
        }
    }

    private void updateTunnel(){
        if(!hasPortal)return;
        for(int i=0;i<tunnelLength;i++){
            tunnelX[i].x = (float) (Math.cos(Math.toRadians(tunnelParams[i].z))*tunnelParams[i].x);
            tunnelX[i].y = 0;
            tunnelX[i].z = (float) (Math.sin(Math.toRadians(tunnelParams[i].z))*tunnelParams[i].x);

            tunnelY[i].x = 0;
            tunnelY[i].y = tunnelParams[i].y;
            tunnelY[i].z = 0;
        }
    }

    public void drawTunnel(BufferBuilder buf) {
        if(!hasPortal)return;
        initTunnel();
        float offset = (float) (counter*0.1);
        int n = (int)counter/3;
        if(n>tunnelLength)n=tunnelLength;
        for(int i=0;i<n-1;i++){
            for(int j=0;j<4;j++){
                int jj = (j+1)%4;
                vecs[0].x = (float) (sqX[j]*0.5*tunnelX[i].x + sqY[j]*0.5*tunnelY[i].x + tunnelPoints[i].x);
                vecs[0].y = (float) (sqX[j]*0.5*tunnelX[i].y + sqY[j]*0.5*tunnelY[i].y + tunnelPoints[i].y);
                vecs[0].z = (float) (sqX[j]*0.5*tunnelX[i].z + sqY[j]*0.5*tunnelY[i].z + tunnelPoints[i].z);
                uvs[0].x = 0;
                uvs[0].y = i + offset;

                vecs[1].x = (float) (sqX[jj]*0.5*tunnelX[i].x + sqY[jj]*0.5*tunnelY[i].x + tunnelPoints[i].x);
                vecs[1].y = (float) (sqX[jj]*0.5*tunnelX[i].y + sqY[jj]*0.5*tunnelY[i].y + tunnelPoints[i].y);
                vecs[1].z = (float) (sqX[jj]*0.5*tunnelX[i].z + sqY[jj]*0.5*tunnelY[i].z + tunnelPoints[i].z);
                uvs[1].x = 1;
                uvs[1].y = i + offset;

                vecs[2].x = (float) (sqX[jj]*0.5*tunnelX[i+1].x + sqY[jj]*0.5*tunnelY[i+1].x + tunnelPoints[i+1].x);
                vecs[2].y = (float) (sqX[jj]*0.5*tunnelX[i+1].y + sqY[jj]*0.5*tunnelY[i+1].y + tunnelPoints[i+1].y);
                vecs[2].z = (float) (sqX[jj]*0.5*tunnelX[i+1].z + sqY[jj]*0.5*tunnelY[i+1].z + tunnelPoints[i+1].z);
                uvs[2].x = 1;
                uvs[2].y = (float) i + 1 + offset;

                vecs[3].x = (float) (sqX[j]*0.5*tunnelX[i+1].x + sqY[j]*0.5*tunnelY[i+1].x + tunnelPoints[i+1].x);
                vecs[3].y = (float) (sqX[j]*0.5*tunnelX[i+1].y + sqY[j]*0.5*tunnelY[i+1].y + tunnelPoints[i+1].y);
                vecs[3].z = (float) (sqX[j]*0.5*tunnelX[i+1].z + sqY[j]*0.5*tunnelY[i+1].z + tunnelPoints[i+1].z);
                uvs[3].x = 0;
                uvs[3].y = (float) i + 1 + offset;

                for(int k=0;k<4;k++){
                    buf.vertex(vecs[k].x, vecs[k].y, vecs[k].z).texture(uvs[k].x, uvs[k].y).color(1f, 1f, 1f, 0.5f);
                }
            }
        }
    }

    private static final float[] sqX = {-1, 1, 1,-1};
    private static final float[] sqY = {-1,-1, 1, 1};

    /*TODO
    @Override
    @OnlyIn(Dist.CLIENT)
    public net.minecraft.util.math.AxisAlignedBB getRenderBoundingBox()
    {
        return INFINITE_EXTENT_AABB;
    }
    */

    public void barrierBlocks(boolean add) {
        if(this.world.isClient)return;

        if(add){
            for(int x=1;x<oldPortalWidth-1;x++)
            for(int y=1;y<oldPortalHeight-1;y++){
                float offsetX = 0;
                float offsetZ = 0;
                if(oldPortalFacing == Direction.WEST){
                    offsetX = 1;
                    offsetZ = x;
                }
                if(oldPortalFacing == Direction.EAST){
                    offsetX = -1;
                    offsetZ = x;
                }
                if(oldPortalFacing == Direction.NORTH){
                    offsetX = x;
                    offsetZ = 1;
                }
                if(oldPortalFacing == Direction.SOUTH){
                    offsetX = x;
                    offsetZ = -1;
                }
                this.world.setBlockState(oldPortalRoot.add(offsetX, y, offsetZ), Blocks.BARRIER.getDefaultState(), 3);
            }
        }
        else{
            for(int x=1;x<oldPortalWidth-1;x++)
            for(int y=1;y<oldPortalHeight-1;y++){
                float offsetX = 0;
                float offsetZ = 0;
                if(oldPortalFacing == Direction.WEST){
                    offsetX = 1;
                    offsetZ = x;
                }
                if(oldPortalFacing == Direction.EAST){
                    offsetX = -1;
                    offsetZ = x;
                }
                if(oldPortalFacing == Direction.NORTH){
                    offsetX = x;
                    offsetZ = 1;
                }
                if(oldPortalFacing == Direction.SOUTH){
                    offsetX = x;
                    offsetZ = -1;
                }
                this.world.setBlockState(oldPortalRoot.add(offsetX, y, offsetZ), Blocks.AIR.getDefaultState(), 3);
            }
        }
    }

    public void portalBlocks(boolean add) {
        if(this.world.isClient)return;

        if(add){
            for(int x=1;x<oldPortalWidth-1;x++)
            for(int y=1;y<oldPortalHeight-1;y++){
                float offsetX = 0;
                float offsetZ = 0;
                if(oldPortalFacing == Direction.WEST ||
                   oldPortalFacing == Direction.EAST){
                    offsetZ = x;
                }
                if(oldPortalFacing == Direction.NORTH ||
                   oldPortalFacing == Direction.SOUTH){
                    offsetX = x;
                }
                this.world.setBlockState(oldPortalRoot.add(offsetX, y, offsetZ), RealmShiftBlocks.REALM_IGNITER_PORTAL.getDefaultState(), 3);
            }
        }
        else{
            for(int x=1;x<oldPortalWidth-1;x++)
            for(int y=1;y<oldPortalHeight-1;y++){
                float offsetX = 0;
                float offsetZ = 0;
                if(oldPortalFacing == Direction.WEST ||
                   oldPortalFacing == Direction.EAST){
                    offsetZ = x;
                }
                if(oldPortalFacing == Direction.NORTH ||
                   oldPortalFacing == Direction.SOUTH){
                    offsetX = x;
                }
                this.world.setBlockState(oldPortalRoot.add(offsetX, y, offsetZ), Blocks.AIR.getDefaultState(), 3);
            }
        }
    }
}
