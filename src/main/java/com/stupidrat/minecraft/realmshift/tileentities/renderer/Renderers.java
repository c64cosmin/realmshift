package com.stupidrat.minecraft.realmshift.tileentities.renderer;

import com.stupidrat.minecraft.realmshift.tileentities.RealmShiftBlockEntities;

import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;

public class Renderers {
    public static void registerRendering()
    {
        BlockEntityRendererRegistry.INSTANCE.register(RealmShiftBlockEntities.FORGED_LAPIS_PORTAL, RenderForgedLapisPortal::new);
        BlockEntityRendererRegistry.INSTANCE.register(RealmShiftBlockEntities.REALM_IGNITER_PORTAL, RenderRealmIgniter::new);
        //ClientRegistry.bindTileEntitySpecialRenderer(RealmIgniterTileEntity.class, new RendererRealmIgniter());
        //ClientRegistry.bindTileEntitySpecialRenderer(ForgedLapisPortalTileEntity.class, new RendererForgedLapisPortal());
    }
}