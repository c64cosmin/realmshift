
package com.stupidrat.minecraft.realmshift.tileentities;

import com.stupidrat.minecraft.realmshift.blocks.ForgedLapis;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;

public class ForgedLapisPortalEntity extends BlockEntity implements Tickable{
    //private static final AxisAlignedBB LAPIS_PORTAL_AABB = new AxisAlignedBB(0,0,0,2,2,2);
    private int counter;

    public ForgedLapisPortalEntity(){
        super(RealmShiftBlockEntities.FORGED_LAPIS_PORTAL);
        counter = 0;
    }

    @Override
    public void tick() {
        if(!this.world.isClient){
            counter++;
            if(counter == 10){
                if(((ForgedLapis) RealmShiftBlocks.FORGED_LAPIS).getPortal(this.world, this.pos.add(-1, -1,-1), false) == null){
                    BlockPos root = ((ForgedLapis) RealmShiftBlocks.FORGED_LAPIS).getPortal(this.world, this.pos.add(-1, -1,-1), true);
                    if(root != null)
                        ((ForgedLapis) RealmShiftBlocks.FORGED_LAPIS).removePortal(world, root);
                }
                counter=0;
            }else if(counter > 10) {
                counter = 10;
            }
        }
    }

    /*TODO
     * @Override
    @OnlyIn(Dist.CLIENT)
    public net.minecraft.util.math.AxisAlignedBB getRenderBoundingBox()
    {
        return LAPIS_PORTAL_AABB.offset(pos);
    }*/
}
