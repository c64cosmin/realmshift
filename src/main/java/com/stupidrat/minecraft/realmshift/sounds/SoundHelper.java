/*
package com.stupidrat.minecraft.realmshift.sounds;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class SoundHelper {
    public static SimpleSound playSound(double x, double y, double z, SoundEvent soundIn, SoundCategory category, float volume, float pitch, boolean distanceDelay) {
        double d0 = Minecraft.getInstance().gameRenderer.getActiveRenderInfo().getProjectedView().squareDistanceTo(x, y, z);
        SimpleSound simplesound = new SimpleSound(soundIn, category, volume, pitch, (float)x, (float)y, (float)z);
        if (distanceDelay && d0 > 100.0D) {
           double d1 = Math.sqrt(d0) / 40.0D;
           Minecraft.getInstance().getSoundHandler().playDelayed(simplesound, (int)(d1 * 20.0D));
        } else {
            Minecraft.getInstance().getSoundHandler().play(simplesound);
        }
        return simplesound;
     }
    
    public static void stopSound(SimpleSound sound) {
        Minecraft.getInstance().getSoundHandler().stop(sound);
    }
}
*/