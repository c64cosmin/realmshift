
package com.stupidrat.minecraft.realmshift.items.nemosil;

import com.stupidrat.minecraft.realmshift.items.RealmShiftArmor;
import com.stupidrat.minecraft.realmshift.items.RealmShiftArmorMaterial;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;

public class NeblineArmorFeet extends RealmShiftArmor {
    public NeblineArmorFeet(){
        super("nemosil_nebline_armor_feet", RealmShiftArmorMaterial.NEBLINE, EquipmentSlot.FEET, (new Item.Settings()).maxCount(1));
    }
}
