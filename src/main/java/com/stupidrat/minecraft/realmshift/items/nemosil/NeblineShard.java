
package com.stupidrat.minecraft.realmshift.items.nemosil;

import com.stupidrat.minecraft.realmshift.items.RealmShiftItem;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class NeblineShard extends RealmShiftItem {
    public NeblineShard(){
        super((new Item.Settings()).maxCount(64).group(ItemGroup.MATERIALS), "nemosil_nebline_shard");
    }
}
