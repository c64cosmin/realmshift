package com.stupidrat.minecraft.realmshift.items;

import net.minecraft.util.Identifier;

public interface IRealmShiftItem {
	public Identifier getIdentifier();
}
