package com.stupidrat.minecraft.realmshift.blocks;

import com.stupidrat.minecraft.realmshift.dimensions.RealmShiftDimensions;
import com.stupidrat.minecraft.realmshift.tileentities.RealmShiftBlockEntities;

import net.fabricmc.fabric.api.dimension.v1.FabricDimensions;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;

public class ForgedLapisPortal extends RealmShiftPortal implements BlockEntityProvider{
    public ForgedLapisPortal(){
        super(FabricBlockSettings.of(Material.AIR, MaterialColor.BLACK), "forged_lapis_portal");
    }
    public static final VoxelShape LAPIS_PORTAL_SHAPE = Block.createCuboidShape(0,0,0,1,0.1,1);

    @Override
    public BlockEntity createBlockEntity(BlockView blockView) {
        return RealmShiftBlockEntities.FORGED_LAPIS_PORTAL.instantiate();
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return ForgedLapisPortal.LAPIS_PORTAL_SHAPE;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onEntityCollision(BlockState state, World world, BlockPos pos, Entity entityIn) {
        if(!world.isClient) {
            if((entityIn.getPos().y - entityIn.lastRenderY) < -1){
                if(entityIn instanceof PlayerEntity){
                    PlayerEntity player = (PlayerEntity) entityIn;
                    ServerWorld serverWorld = (ServerWorld) player.getEntityWorld();
                    RegistryKey<World> destination = World.OVERWORLD;
                    if(serverWorld.getRegistryKey() == World.OVERWORLD)destination = RealmShiftDimensions.NEMOSIL;
                    if(destination == World.OVERWORLD) {
                        FabricDimensions.teleport(player, serverWorld.getServer().getWorld(destination));
                    }else {
                        ServerWorld newWorld = serverWorld.getServer().getWorld(destination);
                        if(newWorld == null) {
                            System.out.println("Oups bad bad!");
                            return;
                        }
                        FabricDimensions.teleport(player, newWorld);
                    }
                }
            }
            else{
                ForgedLapis b = (ForgedLapis)RealmShiftBlocks.FORGED_LAPIS;
                BlockPos root = b.getPortalRoot(world, pos);
                b.removePortal(world, root);
            }
        }
    }
}
