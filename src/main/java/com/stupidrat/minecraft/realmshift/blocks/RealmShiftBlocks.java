package com.stupidrat.minecraft.realmshift.blocks;

import java.util.HashMap;

import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilCeglium;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilCoal;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilCobble;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilDirt;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilFeverDew;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilGrass;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilMusaDoor;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilMusaLeaves;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilMusaLog;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilMusaSapling;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilMusaTrapDoor;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilMusaWood;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilNebline;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilRock;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilRockSlab;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilStone;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilTallGrass;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilViperBerry;

import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

public class RealmShiftBlocks {
    public static final Block POWDERED_END_STONE = new PowderedEndStone();
    public static final Block FORGED_END_STONE = new ForgedEndStone();
    public static final Block REALM_IGNITER = new RealmIgniter();
    public static final Block REALM_IGNITER_PORTAL = new RealmIgniterPortal();
    public static final Block POWDERED_IRON_ORE = new PowderedIronOre();
    public static final Block FORGED_LAPIS = new ForgedLapis();
    public static final Block FORGED_LAPIS_PORTAL = new ForgedLapisPortal();
    public static final Block FORGED_LAPIS_PORTAL_DUMMY = new ForgedLapisPortalDummy();
    public static final Block NEMOSIL_DIRT = new NemosilDirt();
    public static final Block NEMOSIL_STONE = new NemosilStone();
    public static final Block NEMOSIL_ROCK = new NemosilRock();
    public static final Block NEMOSIL_ROCK_SLAB = new NemosilRockSlab();
    public static final Block NEMOSIL_GRASS = new NemosilGrass();
    public static final Block NEMOSIL_NEBLINE = new NemosilNebline();
    public static final Block NEMOSIL_CEGLIUM = new NemosilCeglium();
    public static final Block NEMOSIL_TALL_GRASS = new NemosilTallGrass();
    public static final Block NEMOSIL_VIPER_BERRY = new NemosilViperBerry();
    public static final Block NEMOSIL_FEVERDEW = new NemosilFeverDew();
    public static final Block NEMOSIL_COAL = new NemosilCoal();
    public static final Block NEMOSIL_COBBLESTONE = new NemosilCobble();
    public static final Block NEMOSIL_MUSA_LOG = new NemosilMusaLog();
    public static final Block NEMOSIL_MUSA_SAPLING = new NemosilMusaSapling();
    public static final Block NEMOSIL_MUSA_LEAVES = new NemosilMusaLeaves();
    public static final Block NEMOSIL_MUSA_WOOD = new NemosilMusaWood();
    public static final Block NEMOSIL_MUSA_DOOR = new NemosilMusaDoor();
    public static final Block NEMOSIL_MUSA_TRAPDOOR = new NemosilMusaTrapDoor();

    public static class ItemBlocks {
        public static final HashMap<Block, Item> map = new HashMap<Block, Item>();
    }

    public static void onBlocksRegistry(){
        Block[] blocks = {
             POWDERED_END_STONE
            ,FORGED_END_STONE
            ,REALM_IGNITER
            ,REALM_IGNITER_PORTAL
            ,POWDERED_IRON_ORE
            ,FORGED_LAPIS
            ,FORGED_LAPIS_PORTAL
            ,FORGED_LAPIS_PORTAL_DUMMY
            ,NEMOSIL_DIRT
            ,NEMOSIL_STONE
            ,NEMOSIL_ROCK
            ,NEMOSIL_GRASS
            ,NEMOSIL_NEBLINE
            ,NEMOSIL_CEGLIUM
            ,NEMOSIL_TALL_GRASS
            ,NEMOSIL_VIPER_BERRY
            ,NEMOSIL_FEVERDEW
            ,NEMOSIL_COAL
            ,NEMOSIL_COBBLESTONE
            ,NEMOSIL_ROCK_SLAB
            ,NEMOSIL_MUSA_LOG
            ,NEMOSIL_MUSA_LEAVES
            ,NEMOSIL_MUSA_SAPLING
            ,NEMOSIL_MUSA_WOOD
            ,NEMOSIL_MUSA_DOOR
            ,NEMOSIL_MUSA_TRAPDOOR
        };
        
        for(Block block : blocks){
            Registry.register(Registry.BLOCK, ((IRealmShiftBlock)block).getIdentifier(), block);
            BlockRenderLayerMap.INSTANCE.putBlock(block, ((IRealmShiftBlock)block).getRenderLayer());
        }
    }

    public static void registerItemBlocks() {
        final Block[] items = {
             POWDERED_END_STONE
            ,FORGED_END_STONE
            ,REALM_IGNITER
            ,POWDERED_IRON_ORE
            ,FORGED_LAPIS
            ,NEMOSIL_DIRT
            ,NEMOSIL_STONE
            ,NEMOSIL_ROCK
            ,NEMOSIL_GRASS
            ,NEMOSIL_NEBLINE
            ,NEMOSIL_CEGLIUM
            ,NEMOSIL_TALL_GRASS
            ,NEMOSIL_VIPER_BERRY
            ,NEMOSIL_FEVERDEW
            ,NEMOSIL_COAL
            ,NEMOSIL_COBBLESTONE
            ,NEMOSIL_ROCK_SLAB
            ,NEMOSIL_MUSA_LOG
            ,NEMOSIL_MUSA_LEAVES
            ,NEMOSIL_MUSA_SAPLING
            ,NEMOSIL_MUSA_WOOD
            ,NEMOSIL_MUSA_DOOR
            ,NEMOSIL_MUSA_TRAPDOOR
        };

        for (final Block block : items) {
            registerItemBlock(block);
        }
    }

    public static void registerItemBlock(Block block){
        BlockItem blockItem = new BlockItem(block, new Item.Settings().group(((IGroupedBlock)block).getGroup()));

        Registry.register(Registry.ITEM, ((IRealmShiftBlock)block).getIdentifier(), blockItem);
        ItemBlocks.map.put(block, blockItem);
    }
}
