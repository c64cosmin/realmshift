package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftSapling;
import com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil.MusaTreeGenerator;

import net.minecraft.block.BlockState;

public class NemosilMusaSapling extends RealmShiftSapling{
    public NemosilMusaSapling() {
        super("nemosil_musa_sapling", new BlockState[] {RealmShiftBlocks.NEMOSIL_GRASS.getDefaultState(),
                                                        RealmShiftBlocks.NEMOSIL_DIRT.getDefaultState(),
                                                        RealmShiftBlocks.NEMOSIL_MUSA_LOG.getDefaultState()},
              new MusaTreeGenerator());
    }
}
