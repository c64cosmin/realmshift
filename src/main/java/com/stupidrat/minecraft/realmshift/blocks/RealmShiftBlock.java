package com.stupidrat.minecraft.realmshift.blocks;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.registry.FlammableBlockRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;

public class RealmShiftBlock extends Block implements IRealmShiftBlock, IGroupedBlock{
    public final String name;
    
    public enum RenderType {
        FULL,
        WITH_FANCY,
        MIPPED,
        CUTOUT
    }
    public RenderType renderType = RenderType.FULL;

    public RealmShiftBlock(FabricBlockSettings fabricBlockSettings, String name) {
        super(fabricBlockSettings);

        this.name = name;
    }
    
    public Identifier getIdentifier() {
    	return new Identifier(RealmShiftMod.MODID, this.name);
    }
    
    public ItemGroup getGroup() {
        return ItemGroup.BUILDING_BLOCKS;
    }
    
	@Override
	public RenderLayer getRenderLayer() {
	        switch(this.renderType){
	        case FULL:
	            return RenderLayer.getSolid();
	        case WITH_FANCY:
	            //if(MinecraftClient.getInstance().isFancyGraphicsEnabled()){
	                return RenderLayer.getTranslucent();
	            //}else{
	            //    return RenderLayer.getSolid();
	            //}
	        case MIPPED:
	            return RenderLayer.getCutoutMipped();
	        case CUTOUT:
	            return RenderLayer.getCutout();
	        default:
	            return RenderLayer.getSolid();
	    }
	}
	
	
	public void registerFlammability(int burn, int spread) {
		FlammableBlockRegistry.getDefaultInstance().add(this, burn, spread);
	}
}
