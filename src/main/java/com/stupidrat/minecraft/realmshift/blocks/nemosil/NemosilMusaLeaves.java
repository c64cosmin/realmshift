package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import java.util.Random;

import com.stupidrat.minecraft.realmshift.RealmShiftUtil;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftLeaves;

import net.minecraft.block.BlockState;
import net.minecraft.block.MaterialColor;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

public class NemosilMusaLeaves extends RealmShiftLeaves{
    public NemosilMusaLeaves() {
        super(MaterialColor.BLUE, "nemosil_musa_leaves");
    }
    
    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random rand) {
        for(int i=0;i<6;i++){
            BlockState blockState = world.getBlockState(pos.add(RealmShiftUtil.OFFSET_X[i],
                                                                RealmShiftUtil.OFFSET_Y[i],
                                                                RealmShiftUtil.OFFSET_Z[i]));
            //it's connected
            if(blockState.getBlock() == RealmShiftBlocks.NEMOSIL_MUSA_LOG){
                return;
            }
        }
        if(world.random.nextInt(4) < 3){
            world.breakBlock(pos, true);
        }
    }
}
