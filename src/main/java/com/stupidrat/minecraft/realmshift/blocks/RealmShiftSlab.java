package com.stupidrat.minecraft.realmshift.blocks;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.SlabBlock;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;

public abstract class RealmShiftSlab extends SlabBlock implements IRealmShiftBlock, IGroupedBlock{
    public String name;

	public RealmShiftSlab(FabricBlockSettings settings, String name) {
        super(settings);
        
        this.name = name;
    }
    
    public ItemGroup getGroup() {
        return ItemGroup.BUILDING_BLOCKS;
    }
    
    public Identifier getIdentifier() {
    	return new Identifier(RealmShiftMod.MODID, this.name);
    }
    
    public RenderLayer getRenderLayer() {
    	return RenderLayer.getCutout();
    }
}
