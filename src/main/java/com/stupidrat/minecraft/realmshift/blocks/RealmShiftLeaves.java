package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.EntityShapeContext;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.block.ShapeContext;
import net.minecraft.item.ItemGroup;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class RealmShiftLeaves extends RealmShiftPlant{
    public RealmShiftLeaves(MaterialColor color, String name) {
        super(FabricBlockSettings.of(Material.LEAVES, color).strength(0.2f, 0).sounds(BlockSoundGroup.GRASS).nonOpaque().ticksRandomly(), name, null, RealmShiftBlock.RenderType.WITH_FANCY);
        
        registerFlammability(30, 60);
    }
    
    public ItemGroup getGroup() {
        return ItemGroup.DECORATIONS;
    }
    
    
    @Override
    public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos neighborPos, boolean moved) {
    }
    
    @Override
    public Block.OffsetType getOffsetType() {
        return Block.OffsetType.NONE;
    }

	@Override
	public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
		return VoxelShapes.fullCube();
	}

	@Override
	public int getOpacity(BlockState state, BlockView view, BlockPos pos) {
		return 1;
	}
}
