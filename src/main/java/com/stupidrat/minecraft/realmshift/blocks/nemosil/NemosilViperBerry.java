package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlock;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftPlant;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.item.ItemGroup;

public class NemosilViperBerry extends RealmShiftPlant{
	public NemosilViperBerry() {
		super(FabricBlockSettings.of(Material.REPLACEABLE_PLANT, MaterialColor.BLUE).strength(0, 0).collidable(false), "nemosil_viper_berry",
			  new BlockState[] {
					  RealmShiftBlocks.NEMOSIL_GRASS.getDefaultState(),
					  RealmShiftBlocks.NEMOSIL_DIRT.getDefaultState()
		      },
			  RealmShiftBlock.RenderType.CUTOUT);
    }
    
    public ItemGroup getGroup() {
        return ItemGroup.DECORATIONS;
    }

    @Override
    public Block.OffsetType getOffsetType() {
        return Block.OffsetType.XZ;
     }
}
