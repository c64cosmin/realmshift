package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;

public class PowderedIronOre extends RealmShiftBlock{
    public PowderedIronOre(){
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.STONE).strength(4, 15), "powdered_iron_ore");
    }
}
