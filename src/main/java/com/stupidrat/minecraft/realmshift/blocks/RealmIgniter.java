package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.Direction;

public class RealmIgniter extends RealmShiftBlock{// implements ITileEntityProvider{
    public RealmIgniter() {
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.STONE).strength(4, 15), "realm_igniter");
        
        this.setDefaultState(this.getDefaultState()
                			     .with(FACING, Direction.EAST)
                			     .with(ACTIVE, false));
    }
    
    public static final BooleanProperty ACTIVE = Properties.LIT;
    public static final DirectionProperty FACING = Properties.HORIZONTAL_FACING;

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
    	stateManager.add(ACTIVE);
    	stateManager.add(FACING);
    }

    public BlockState getPlacementState(ItemPlacementContext ctx) {
       return (BlockState)this.getDefaultState().with(FACING, ctx.getPlayerFacing().getOpposite());
    }
/*
    public void activatePortal(World worldIn, RealmIgniterTileEntity te, BlockPos pos, BlockState state) {
        worldIn.setBlockState(pos, state.with(ACTIVE, true), 3);
        worldIn.playSound((PlayerEntity)null, pos, SoundEvents.ENTITY_DRAGON_FIREBALL_EXPLODE, SoundCategory.BLOCKS, 4.0F, 0.5F);
        worldIn.playSound((PlayerEntity)null, pos, SoundEvents.BLOCK_BEACON_ACTIVATE, SoundCategory.BLOCKS, 4.0F, 0.5F);
        for(int i=0;i<10;i++){
            worldIn.addParticle(ParticleTypes.EXPLOSION,
                                pos.getX()-1+worldIn.random.nextDouble()*3,
                                pos.getY()-1+worldIn.random.nextDouble()*3,
                                pos.getZ()-1+worldIn.random.nextDouble()*3,
                                0,0,0);
            worldIn.addParticle(ParticleTypes.FLAME, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, worldIn.random.nextDouble()*0.2-0.1, worldIn.random.nextDouble()*0.2-0.1, worldIn.random.nextDouble()*0.2-0.1);
        }
        te.counter = 0;
        te.initSound = true;
        te.barrierBlocks(true);
        te.portalBlocks(true);
    }
    
    public void deactivatePortal(World worldIn, RealmIgniterTileEntity te, BlockPos pos, BlockState state) {
        worldIn.setBlockState(pos, state.with(ACTIVE, false), 3);
        worldIn.playSound((PlayerEntity)null, pos, SoundEvents.ENTITY_DRAGON_FIREBALL_EXPLODE, SoundCategory.BLOCKS, 4.0F, 0.5F);
        //worldIn.playSound((PlayerEntity)null, pos, SoundEvents.BLOCK_BEACON_DEACTIVATE, SoundCategory.BLOCKS, 4.0F, 0.5F);
        worldIn.playSound((PlayerEntity)null, pos, SoundEvents.BLOCK_BEACON_DEACTIVATE, SoundCategory.BLOCKS, 4.0F, 0.3F);
        for(int i=0;i<10;i++){
            worldIn.addParticle(ParticleTypes.EXPLOSION,
                    pos.getX()-1+worldIn.random.nextDouble()*3,
                    pos.getY()-1+worldIn.random.nextDouble()*3,
                    pos.getZ()-1+worldIn.random.nextDouble()*3,
                    0,0,0);
            worldIn.addParticle(ParticleTypes.FLAME, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, worldIn.random.nextDouble()*0.2-0.1, worldIn.random.nextDouble()*0.2-0.1, worldIn.random.nextDouble()*0.2-0.1);
        }
        te.counter = 0;
        te.initSound = true;
        te.stopSounds();
        te.barrierBlocks(false);
        te.portalBlocks(false);
    }
/*
    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity playerIn, Hand handIn, BlockRayTraceResult hit)
    {
        if(!worldIn.isClient){
            RealmIgniterTileEntity te = (RealmIgniterTileEntity)(worldIn.getTileEntity(pos));
            ItemStack is = playerIn.inventory.getCurrentItem();
            if(worldIn.getBlockState(pos).get(ACTIVE) == false)
            if(is != null)
            if(is.getItem() != null)
            if(is.getItem() == RealmShiftItems.SHULKER_KEY){
                if(isIgniterAround(worldIn, pos))return false;
                if(te.hasPortal){
                    is.shrink(1);
                    if(is.getCount() <= 0)
                        playerIn.inventory.setInventorySlotContents(playerIn.inventory.currentItem, ItemStack.EMPTY);

                    activatePortal(worldIn, te, pos, state);
                }
                return true;
            }
            if(worldIn.getBlockState(pos).get(ACTIVE) == true){
                deactivatePortal(worldIn, te, pos, state);
                return true;
            }
            worldIn.setBlockState(pos, state.with(FACING, Direction.fromAngle(hit.getFace().getHorizontalAngle())), 3);
        }else{
            if(worldIn.getBlockState(pos).get(ACTIVE) == false) {
                RealmIgniterTileEntity te = (RealmIgniterTileEntity)(worldIn.getTileEntity(pos));
                ItemStack is = playerIn.inventory.getCurrentItem();
                if(worldIn.getBlockState(pos).get(ACTIVE) == false)
                if(is != null)
                if(is.getItem() != null)
                if(is.getItem() == RealmShiftItems.SHULKER_KEY) {
                    if(isIgniterAround(worldIn, pos))return false;
                    if(te.hasPortal){
                        activatePortal(worldIn, te, pos, state);
                    }
                }
            }
            if(worldIn.getBlockState(pos).get(ACTIVE) == true){
                RealmIgniterTileEntity te = (RealmIgniterTileEntity)(worldIn.getTileEntity(pos));
                deactivatePortal(worldIn, te, pos, state);
                return true;
            }
        }
        return true;
    }

    private boolean isIgniterAround(World worldIn, BlockPos pos) {
        for(int x=-20;x<=20;x++)
        for(int y=-20;y<=20;y++)
        for(int z=-20;z<=20;z++){
            BlockState bs = worldIn.getBlockState(pos.add(x, y, z));
            if(bs.getBlock() == RealmShiftBlocks.REALM_IGNITER)
            if(bs.get(ACTIVE)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, BlockState state, PlayerEntity player)
    {
        if(!worldIn.isClient){
            if(state.get(ACTIVE) == true){
                RealmIgniterTileEntity te = (RealmIgniterTileEntity)(worldIn.getTileEntity(pos));
                te.barrierBlocks(false);
                te.portalBlocks(false);
            }
        }
        super.onBlockHarvested(worldIn, pos, state, player);
    }

    @Override
    public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosion)
    {
        if(!worldIn.isClient){
            if(worldIn.getBlockState(pos).get(ACTIVE) == true){
                RealmIgniterTileEntity te = (RealmIgniterTileEntity)(worldIn.getTileEntity(pos));
                te.barrierBlocks(false);
                te.portalBlocks(false);
            }
        }
        worldIn.destroyBlock(pos, false);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING, ACTIVE);
    }

    @Override
    public boolean hasTileEntity(BlockState state)
    {
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
        return new RealmIgniterTileEntity();
    }

    @Override
    public BlockRenderType getRenderType(BlockState state)
    {
        return BlockRenderType.MODEL;
    }

    @Override
    public float getBlockHardness(BlockState blockState, IBlockReader worldIn, BlockPos pos)
    {
        if(blockState.get(ACTIVE))return -1;
        return this.blockHardness;
    }

    @Override
    public int getLightValue(BlockState blockState)
    {
        if(blockState.get(ACTIVE))return 15;
        return 0;
    }
    */
}
