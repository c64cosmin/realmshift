package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlock;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.sound.BlockSoundGroup;

public class NemosilNebline extends RealmShiftBlock {
    public NemosilNebline() {
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.GREEN)
                .strength(3, 6)
                .sounds(BlockSoundGroup.STONE)
                .breakByTool(FabricToolTags.PICKAXES),
                "nemosil_nebline");
    }
}
