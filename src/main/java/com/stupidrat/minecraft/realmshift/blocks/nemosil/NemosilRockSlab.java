package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftSlab;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.sound.BlockSoundGroup;

public class NemosilRockSlab extends RealmShiftSlab {
    public NemosilRockSlab() {
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.PURPLE)
                .strength(2, 10)
                .sounds(BlockSoundGroup.STONE)
                .breakByTool(FabricToolTags.PICKAXES),
                "nemosil_rock_slab");
    }
}
