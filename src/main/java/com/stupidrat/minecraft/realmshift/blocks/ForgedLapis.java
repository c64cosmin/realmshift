package com.stupidrat.minecraft.realmshift.blocks;

import java.util.Random;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.explosion.Explosion;

public class ForgedLapis extends RealmShiftBlock{
    public ForgedLapis(){
        super(FabricBlockSettings.of(Material.STONE, MaterialColor.STONE).strength(4, 15).breakByTool(FabricToolTags.PICKAXES, 0), "forged_lapis");
    }

    @Override
    public ActionResult onUse(BlockState state, World worldIn, BlockPos pos, PlayerEntity playerIn, Hand handIn, BlockHitResult hit) {
        if(!worldIn.isClient){
            BlockPos portalRoot = getPortal(worldIn, pos, false);
            ItemStack is = playerIn.getStackInHand(handIn);
            if(portalRoot != null &&
               !isActive(worldIn, portalRoot) &&
               is.getItem() == Items.LAPIS_LAZULI){
                addPortal(worldIn, portalRoot);
                if(!playerIn.isCreative()) {
                    is.decrement(1);
                    if (is.getCount() <= 0) {
                        playerIn.inventory.setStack(playerIn.inventory.selectedSlot, ItemStack.EMPTY);
                    }
                }
                return ActionResult.SUCCESS;
            }
        }
        return ActionResult.PASS;
    }

    public boolean isActive(World worldIn, BlockPos portalRoot){
        return worldIn.getBlockState(portalRoot.add(1, 1, 1)).getBlock() == RealmShiftBlocks.FORGED_LAPIS_PORTAL;
    }

    public void addPortal(World worldIn, BlockPos portalRoot) {
        worldIn.setBlockState(portalRoot.add(1, 1, 1), RealmShiftBlocks.FORGED_LAPIS_PORTAL.getDefaultState(), 3);
        worldIn.setBlockState(portalRoot.add(2, 1, 1), RealmShiftBlocks.FORGED_LAPIS_PORTAL_DUMMY.getDefaultState(), 3);
        worldIn.setBlockState(portalRoot.add(2, 1, 2), RealmShiftBlocks.FORGED_LAPIS_PORTAL_DUMMY.getDefaultState(), 3);
        worldIn.setBlockState(portalRoot.add(1, 1, 2), RealmShiftBlocks.FORGED_LAPIS_PORTAL_DUMMY.getDefaultState(), 3);
    }

    public void removePortal(WorldAccess world, BlockPos portalRoot) {
        this.putAirIfPortal(world, portalRoot.add(1, 1, 1));
        this.putAirIfPortal(world, portalRoot.add(2, 1, 1));
        this.putAirIfPortal(world, portalRoot.add(2, 1, 2));
        this.putAirIfPortal(world, portalRoot.add(1, 1, 2));
    }

    private void putAirIfPortal(WorldAccess world, BlockPos pos) {
        if(world.getBlockState(pos).getBlock() == RealmShiftBlocks.FORGED_LAPIS_PORTAL ||
           world.getBlockState(pos).getBlock() == RealmShiftBlocks.FORGED_LAPIS_PORTAL_DUMMY)
        world.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
    }

    public BlockPos getPortal(World worldIn, BlockPos pos, boolean justFrame) {
        BlockPos it = null;

        it = getPortalRoot(worldIn, pos);

        //test if proper portal
        //<it> is still the root
        for(int x=0;x<4;x++)
        for(int y=0;y<4;y++){
            if(worldIn.getBlockState(it.add(x, 0, y)).getBlock() != RealmShiftBlocks.FORGED_LAPIS)return null;
            if(x==0||y==0||x==3||y==3){
                if(worldIn.getBlockState(it.add(x, 1, y)).getBlock() != RealmShiftBlocks.FORGED_LAPIS)return null;
            }
            else{
                if(!justFrame)
                if(worldIn.getBlockState(it.add(x, 1, y)).getBlock() != Blocks.AIR &&
                   worldIn.getBlockState(it.add(x, 1, y)).getBlock() != RealmShiftBlocks.FORGED_LAPIS_PORTAL &&
                   worldIn.getBlockState(it.add(x, 1, y)).getBlock() != RealmShiftBlocks.FORGED_LAPIS_PORTAL_DUMMY)return null;
            }
        }
        return it;
    }

    public BlockPos getPortalRoot(WorldAccess world, BlockPos it) {
        int itCount = 0;
        while(world.getBlockState(it.down()).getBlock() == RealmShiftBlocks.FORGED_LAPIS &&
              itCount < 2){
            itCount++;
            it = it.down();
        }
        itCount = 0;
        while(world.getBlockState(it.north()).getBlock() == RealmShiftBlocks.FORGED_LAPIS &&
                itCount < 5){
            itCount++;
            it = it.north();
        }
        itCount = 0;
        while(world.getBlockState(it.west()).getBlock() == RealmShiftBlocks.FORGED_LAPIS &&
                itCount < 5){
            itCount++;
            it = it.west();
        }
        return it;
    }

    @Override
    public void onBroken(WorldAccess world, BlockPos pos, BlockState state) {
        if(!world.isClient()){
            BlockPos portalRoot = getPortalRoot(world, pos);
            if(portalRoot != null){
                removePortal(world, portalRoot);
            }
        }
    }

    @Override
    public void onDestroyedByExplosion(World world, BlockPos pos, Explosion explosion) {
        BlockPos portalRoot = getPortalRoot(world, pos);
        if(portalRoot != null){
            removePortal(world, portalRoot);
        }
        world.removeBlock(pos, false);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void randomDisplayTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        BlockPos portalRoot = this.getPortal(worldIn, pos, true);
        if(portalRoot != null){
            worldIn.addParticle(ParticleTypes.END_ROD, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, rand.nextDouble()*0.14-0.07, rand.nextDouble()*0.14-0.07, rand.nextDouble()*0.14-0.07);
        }
    }
}
