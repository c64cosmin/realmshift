package com.stupidrat.minecraft.realmshift.blocks;

import java.util.Random;

import com.stupidrat.minecraft.realmshift.dimensions.generators.IFeatureGenerator;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Fertilizable;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.item.ItemGroup;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class RealmShiftSapling extends RealmShiftPlant implements IPlantable, Fertilizable{
    protected static final VoxelShape SAPLING_SHAPE = Block.createCuboidShape(0.15D, 0.0D, 0.15D, 0.85D, 1.0D, 0.85D);
    public IFeatureGenerator generator;
	public int lightLevelRequired;
	public int growthChanceModifier;

    public RealmShiftSapling(FabricBlockSettings settings, String name, BlockState[] placeableOn, int lightLevelRequired, int growthChanceModifier, IFeatureGenerator generator) {
        super(settings.collidable(false).sounds(BlockSoundGroup.GRASS).nonOpaque().ticksRandomly(), name, placeableOn, RealmShiftBlock.RenderType.CUTOUT);

        this.generator = generator;
        this.growthChanceModifier = growthChanceModifier;
        this.lightLevelRequired = lightLevelRequired;
    }
    
    public RealmShiftSapling(String name, BlockState[] placeableOn, IFeatureGenerator generator) {
    	this(FabricBlockSettings.of(Material.PLANT, MaterialColor.FOLIAGE).strength(0,  0).ticksRandomly(), name, placeableOn, 9, 7, generator);
    }
    
    public ItemGroup getGroup() {
        return ItemGroup.DECORATIONS;
    }
    
    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random rand){
    	if (world.getBaseLightLevel(pos, 0) >= lightLevelRequired && rand.nextInt(growthChanceModifier) == 0)
        {
            this.grow(world, world.random, pos, state);
        }
    }
    

    public void grow(World worldIn, Random rand, BlockPos pos, BlockState state) {
    }
    
    @Override
    public Block.OffsetType getOffsetType() {
        return Block.OffsetType.NONE;
    }

	@Override
	public boolean isFertilizable(BlockView world, BlockPos pos, BlockState state, boolean isClient) {
		return true;
	}

	@Override
	public boolean canGrow(World world, Random random, BlockPos pos, BlockState state) {
		return world.random.nextFloat() < 0.25;
	}

	@Override
	public void grow(ServerWorld world, Random random, BlockPos pos, BlockState state) {	
		this.generator.generate(world, random, pos);
	}
}
