package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftWood;

public class NemosilMusaWood extends RealmShiftWood {
    public NemosilMusaWood() {
        super("nemosil_musa_wood");
    }
}
