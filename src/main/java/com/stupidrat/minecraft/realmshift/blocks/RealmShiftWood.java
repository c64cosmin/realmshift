package com.stupidrat.minecraft.realmshift.blocks;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.sound.BlockSoundGroup;

public class RealmShiftWood extends RealmShiftBlock {
    public RealmShiftWood(String name) {
        super(FabricBlockSettings.of(Material.WOOD, MaterialColor.WOOD).strength(2, 5).sounds(BlockSoundGroup.WOOD), name);
        
        registerFlammability(5, 5);
    }
}
