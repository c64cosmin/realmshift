package com.stupidrat.minecraft.realmshift.blocks.nemosil;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlock;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftPlant;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.item.ItemGroup;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.StringIdentifiable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class NemosilTallGrass extends RealmShiftPlant{   
    public NemosilTallGrass() {
        super(FabricBlockSettings.of(Material.REPLACEABLE_PLANT, MaterialColor.ORANGE)
                        .strength(0, 0).collidable(false),
                        "nemosil_tall_grass",
          			  	new BlockState[] {
        					  RealmShiftBlocks.NEMOSIL_GRASS.getDefaultState(),
        					  RealmShiftBlocks.NEMOSIL_DIRT.getDefaultState()
        		        },
                        RealmShiftBlock.RenderType.CUTOUT);

        this.setDefaultState(this.getDefaultState().with(GRASS_TYPE, NemosilGrassType.GRASS));
    }

    public static final EnumProperty<NemosilGrassType> GRASS_TYPE = EnumProperty.of("type", NemosilGrassType.class);
    public ItemGroup getGroup() {
        return ItemGroup.DECORATIONS;
    }

    @Override
    public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos neighborPos, boolean moved) {
        if (world.isAir(pos.down()))
        {
            world.breakBlock(pos, false);
        }
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
    	stateManager.add(GRASS_TYPE);
    }

    public static enum NemosilGrassType implements StringIdentifiable{
        GRASS("grass"),
        TOP("top"),
        MIDDLE("middle"),
        BOTTOM("bottom");
        
        private final String name;

        private NemosilGrassType(String name) {
           this.name = name;
        }

        public String toString() {
        	return this.name;
        }

        public String asString() {
        	return this.name;
        }
    }
}
