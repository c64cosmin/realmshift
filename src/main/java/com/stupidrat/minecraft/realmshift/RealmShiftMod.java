package com.stupidrat.minecraft.realmshift;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.dimensions.RealmShiftDimensions;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomes;
import com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil.NemosilBiomeSource;
import com.stupidrat.minecraft.realmshift.items.RealmShiftItems;
import com.stupidrat.minecraft.realmshift.tileentities.RealmShiftBlockEntities;

import net.fabricmc.api.ModInitializer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class RealmShiftMod implements ModInitializer
{
    public static final String MODID = "realmshift";

	@Override
	public void onInitialize() {
		RealmShiftBlocks.onBlocksRegistry();
		RealmShiftBlocks.registerItemBlocks();
		RealmShiftBlockEntities.onRegister();
		RealmShiftItems.registerItems();
		RealmShiftBiomes.registerBiomes();
		RealmShiftDimensions.register();
		Registry.register(Registry.BIOME_SOURCE, new Identifier(RealmShiftMod.MODID,  "nemosil"), NemosilBiomeSource.CODEC);
	}
    
    /*
    public RealmShiftMod() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::preinit);
        // Register the enqueueIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        
        FMLJavaModLoadingContext.get().getModEventBus().addListener(RealmShiftTeleporter::onTeleportation);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    public void preinit(final FMLCommonSetupEvent event){
        RealmShiftDimensions.register();
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        Renderers.registerTERendering();
        PROXY.enableStencil();
    }

    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        // some example code to dispatch IMC to another mod
        // InterModComms.sendTo("examplemod", "helloworld", () -> { LOGGER.info("Hello world from the MDK"); return "Hello world";});
    }

    private void processIMC(final InterModProcessEvent event)
    {
        // some example code to receive and process InterModComms from other mods
        /*LOGGER.info("Got IMC {}", event.getIMCStream().
                map(m->m.getMessageSupplier().get()).
                collect(Collectors.toList()));
    }

    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        // do something when the server starts
        LOGGER.info("HELLO from server starting");
    }
    */
}