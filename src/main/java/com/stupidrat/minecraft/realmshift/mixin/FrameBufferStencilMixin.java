package com.stupidrat.minecraft.realmshift.mixin;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.platform.FramebufferInfo;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.texture.TextureUtil;

@Mixin(Framebuffer.class)
public class FrameBufferStencilMixin {
    public int stencilAttachment;

    @Inject(method = "<init>", at = @At("RETURN"))
    private void afterFrameBufferInit(int width, int height, boolean useDepth, boolean getError, CallbackInfo info) {
        this.stencilAttachment = -1;
    }
    
    @Inject(method = "initFbo",
            at = @At(value="INVOKE",
                     target="Lnet/minecraft/client/gl/Framebuffer;checkFramebufferStatus()V"))
    private void initFboStencil(int width, int height, boolean getError, CallbackInfo info) {
        this.stencilAttachment = TextureUtil.generateId();
        GlStateManager.bindTexture(this.stencilAttachment);
        GlStateManager.texImage2D(GL11.GL_TEXTURE_2D, 0, GL30.GL_STENCIL_INDEX8, width, height, 0, GL11.GL_STENCIL_INDEX, GL11.GL_UNSIGNED_BYTE, null);
        GlStateManager.framebufferTexture2D(FramebufferInfo.FRAME_BUFFER, GL30.GL_STENCIL_ATTACHMENT, GL11.GL_TEXTURE_2D, this.stencilAttachment, 0);
    }
}
