package com.stupidrat.minecraft.realmshift.proxy;

import net.minecraft.item.Item;

public class ServerProxy extends CommonProxy {
    @Override
    public void registerItemRenderer(Item item) {
    }

    @Override
    public void registerItemRendererWithMeta(Item item, int meta, String resource) {
    }

    @Override
    public void enableStencil(){
    }
}
