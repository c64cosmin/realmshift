package com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil;
import java.util.Random;

import com.stupidrat.minecraft.realmshift.RealmShiftUtil;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftLog;
import com.stupidrat.minecraft.realmshift.dimensions.generators.IFeatureGenerator;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.WorldAccess;

public class MusaTreeGenerator implements IFeatureGenerator{

    @Override
    public void generate(ServerWorld world, Random rand, BlockPos pos){
        int branchLenght = rand.nextInt(3)+1;
        int bottomHeight = rand.nextInt(3)+4+branchLenght;
        int branchHang = bottomHeight-4;
        if (!doesTreeFit(world, pos, branchLenght, bottomHeight))return;

        for(int i=0;i<bottomHeight;i++){
            world.setBlockState(pos.up(i), RealmShiftBlocks.NEMOSIL_MUSA_LOG.getDefaultState(), 3);
        }
        /*
        return;
        */
        world.setBlockState(pos.up(bottomHeight), RealmShiftBlocks.NEMOSIL_MUSA_LEAVES.getDefaultState(), 3);

        for(int t=0;t<4;t++){
            for(int i=1;i<=branchLenght;i++){
                BlockState logState= RealmShiftBlocks.NEMOSIL_MUSA_LOG.getDefaultState().with(RealmShiftLog.AXIS, Direction.Axis.X);
                if(t>=2)logState= RealmShiftBlocks.NEMOSIL_MUSA_LOG.getDefaultState().with(RealmShiftLog.AXIS, Direction.Axis.Z);
                BlockPos p = pos.up(bottomHeight).add(RealmShiftUtil.OFFSET_X[t]*i, 0, RealmShiftUtil.OFFSET_Y[t]*i);
                world.setBlockState(p, logState, 3);
                for(int l=0;l<4;l++){
                    int offX = RealmShiftUtil.OFFSET_X[l];
                    int offY = RealmShiftUtil.OFFSET_Y[l];
                    int side = 0;
                    if(t>=2)side = 1;
                    world.setBlockState(p.add(offX*side,offY,offX*(1-side)), RealmShiftBlocks.NEMOSIL_MUSA_LEAVES.getDefaultState(), 3);
                }
            }
        }
        for(int t=0;t<4;t++){
            BlockPos p = pos.up(bottomHeight).add(RealmShiftUtil.OFFSET_X[t]*(branchLenght+1), 0, RealmShiftUtil.OFFSET_Y[t]*(branchLenght+1));
            world.setBlockState(p, RealmShiftBlocks.NEMOSIL_MUSA_LEAVES.getDefaultState(), 3);
            int hang = rand.nextInt(branchHang)+2;
            world.setBlockState(p.down(hang+1), RealmShiftBlocks.NEMOSIL_MUSA_LEAVES.getDefaultState(), 3);
            for(int i=1;i<=hang;i++){
                world.setBlockState(p.down(i), RealmShiftBlocks.NEMOSIL_MUSA_LOG.getDefaultState(), 3);
            }
            for(int l=0;l<4;l++){
                world.setBlockState(p.add(RealmShiftUtil.OFFSET_X[l], 0, RealmShiftUtil.OFFSET_Y[l]).down(), RealmShiftBlocks.NEMOSIL_MUSA_LEAVES.getDefaultState(), 3);
            }
        }
       //*/
    }

    private static boolean doesTreeFit(WorldAccess worldIn, BlockPos pos, int branchLenght, int bottomHeight){
        for(int i=1;i<bottomHeight+1;i++){
            if(worldIn.getBlockState(pos.up(i)) != Blocks.AIR.getDefaultState()){
                return false;
            }
            for(int l=0;l<4;l++){
                if(worldIn.getBlockState(pos.add(RealmShiftUtil.OFFSET_X[l]*branchLenght, i+2, RealmShiftUtil.OFFSET_Y[l]*branchLenght)) != Blocks.AIR.getDefaultState()){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void generate(ServerWorld world, Random rand, BlockPos pos, BlockState state) {
        this.generate(world, rand, pos);
    }
}