package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;

import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class RealmShiftBiomes{
    public static RealmShiftBiome NEMOSIL_HILLS = new BiomeNemosilHills();
    public static RealmShiftBiome NEMOSIL_PLATEAU = new BiomeNemosilPlateau();
    public static RealmShiftBiome NEMOSIL_PLAINS = new BiomeNemosilPlains();

    public static void registerBiomes() {
            RealmShiftBiome[] biomes = {
                NEMOSIL_HILLS,
                NEMOSIL_PLAINS,
                NEMOSIL_PLATEAU
            };

            for(RealmShiftBiome biome : biomes) {
                Registry.register(Registry.BIOME, new Identifier(RealmShiftMod.MODID, biome.registryName), biome);
            }
    }
}
