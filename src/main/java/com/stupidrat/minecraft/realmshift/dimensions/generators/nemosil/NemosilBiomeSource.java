package com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil;

import java.util.List;

import com.flowpowered.noise.module.source.Perlin;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import com.stupidrat.minecraft.realmshift.RealmShiftUtil;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomeSource;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomes;

import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.source.BiomeSource;

public class NemosilBiomeSource extends RealmShiftBiomeSource{
    public static final Codec<NemosilBiomeSource> CODEC = RecordCodecBuilder.create((instance) -> {
        return instance.group(Registry.BIOME.listOf().fieldOf("biomes").forGetter((biomeSource) -> {
           return biomeSource.method_28443();
        })).apply(instance, NemosilBiomeSource::new);
     });

    private Perlin biome0;
    private Perlin biome1;
    private Perlin biome2;

    public NemosilBiomeSource(List<Biome> biomes){
        super(biomes);
        long seed = 0;

        biome0 = new Perlin();
        biome0.setSeed((int) (seed+8));
        biome0.setFrequency(0.004);
        biome0.setOctaveCount(1);
        biome0.setLacunarity(1);
        biome0.setPersistence(1);

        biome1 = new Perlin();
        biome1.setSeed((int) (seed+9));
        biome1.setFrequency(0.005);
        biome1.setOctaveCount(1);
        biome1.setLacunarity(1);
        biome1.setPersistence(1);

        biome2 = new Perlin();
        biome2.setSeed((int) (seed+15));
        biome2.setFrequency(0.03);
        biome2.setOctaveCount(3);
        biome2.setLacunarity(1);
        biome2.setPersistence(1);
    }
    
    public Biome getBiome(int x, int z) {
        Biome biome;
        double density = 0;

        biome = RealmShiftBiomes.NEMOSIL_HILLS;

        density = biome0.getValue(x, 0, z);
        if(density>0){
            biome = RealmShiftBiomes.NEMOSIL_PLATEAU;
        }
        density = biome1.getValue(x, 0, z);
        if(density>0){
            biome = RealmShiftBiomes.NEMOSIL_PLAINS;
        }

        return biome;
    }

    @Override
    public double getBiomesWeight(Biome biome, int x, int z){
        if(biome == RealmShiftBiomes.NEMOSIL_HILLS){
            return 1;
        }
        else if(biome == RealmShiftBiomes.NEMOSIL_PLATEAU){
            return biome0.getValue(x, 0, z);
        }
        else if(biome == RealmShiftBiomes.NEMOSIL_PLAINS){
            return biome1.getValue(x, 0, z);
        }
        return 0;
    }

    public double getTreesWeight(Biome biome, int x, int z){
        if(biome == RealmShiftBiomes.NEMOSIL_HILLS){
            double treeWeight = 0;//biome2.getValue(x, 0, z)-0.2;

            double plateauWeight = getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLATEAU, x, z);
            double plainsWeight = getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLAINS, x, z);

            plateauWeight = RealmShiftUtil.clamp(0, 1, plateauWeight*2);
            plainsWeight = RealmShiftUtil.clamp(0, 1, plainsWeight*2);
            treeWeight = RealmShiftUtil.clamp(0, 1, treeWeight);

            return treeWeight - plateauWeight - plainsWeight;
        }
        else if(biome == RealmShiftBiomes.NEMOSIL_PLATEAU){
            return biome0.getValue(x, 0, z);
        }
        else if(biome == RealmShiftBiomes.NEMOSIL_PLAINS){
            return biome1.getValue(x, 0, z);
        }
        return 0;
    }
    
    @Override
    protected Codec<NemosilBiomeSource> method_28442() {
        return NemosilBiomeSource.CODEC;
    }

    @Override
    public Biome getBiomeForNoiseGen(int x, int y, int z) {
        return this.getBiome(x*4, z*4);
    }

    @Override
    public BiomeSource withSeed(long seed) {
        return this;
    }

}