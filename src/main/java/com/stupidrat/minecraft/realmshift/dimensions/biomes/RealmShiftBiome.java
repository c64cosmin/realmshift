package com.stupidrat.minecraft.realmshift.dimensions.biomes;

import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.surfacebuilder.SurfaceBuilder;

public class RealmShiftBiome extends Biome {
    public String registryName;
    public RealmShiftBiome(String registryName, Category category, Biome.Settings settings) {
    	super(settings.configureSurfaceBuilder(SurfaceBuilder.DEFAULT, SurfaceBuilder.AIR_CONFIG).category(category));
    	
    	this.registryName = registryName;
    }
}
