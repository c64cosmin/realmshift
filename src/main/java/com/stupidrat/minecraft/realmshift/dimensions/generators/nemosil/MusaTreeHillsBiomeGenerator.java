package com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil;

import java.util.Random;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftSapling;
import com.stupidrat.minecraft.realmshift.dimensions.generators.IFeatureGenerator;

import net.minecraft.block.BlockState;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

public class MusaTreeHillsBiomeGenerator implements IFeatureGenerator{
    public MusaTreeHillsBiomeGenerator(){
    }

    @Override
    public void generate(ServerWorld world, Random rand, BlockPos pos){
        //double weight = ((NemosilBiomeSource)biomeSource).getTreesWeight(RealmShiftBiomes.NEMOSIL_HILLS, pos.getX(), pos.getZ());

        int n = 1;
        BlockPos treeBases[] = new BlockPos[n];

        for(int i=0;i<n;i++){
            int offX = 0;//rand.nextInt(5)-2;
            int offY = 0;//rand.nextInt(5)-2;
            BlockPos top = pos.add(offX, 150, offY);
            /*while(!world.getBlockState(pos).getMaterial().isSolid() &&
                  !world.getBlockState(pos).isOpaque())
                top.down();*/
            treeBases[i] = top;
        }
        for(BlockPos root : treeBases)
        if(rand.nextDouble() < 0.05){
            //((RealmShiftSapling) RealmShiftBlocks.NEMOSIL_MUSA_SAPLING).generator.generate(world, rand, root);
        }
    }

    @Override
    public void generate(ServerWorld world, Random rand, BlockPos pos, BlockState state) {
        this.generate(world, rand, pos);
    }
}