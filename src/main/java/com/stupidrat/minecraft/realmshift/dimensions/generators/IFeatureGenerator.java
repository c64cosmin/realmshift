package com.stupidrat.minecraft.realmshift.dimensions.generators;

import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

public interface IFeatureGenerator {
    public void generate(ServerWorld world, Random rand, BlockPos pos, BlockState state);
    public void generate(ServerWorld world, Random rand, BlockPos pos);
}
