package com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil;

import java.util.Random;

import com.flowpowered.noise.module.source.Perlin;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.BlockView;
import net.minecraft.world.ChunkRegion;
import net.minecraft.world.Heightmap.Type;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.source.BiomeSource;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.StructureAccessor;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import com.stupidrat.minecraft.realmshift.RealmShiftUtil;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.nemosil.NemosilTallGrass;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomeSource;
import com.stupidrat.minecraft.realmshift.dimensions.biomes.RealmShiftBiomes;
import com.stupidrat.minecraft.realmshift.dimensions.generators.RealmShiftChunkGenerator;
import com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil.NemosilBiomeSource;

public class NemosilChunkGenerator extends RealmShiftChunkGenerator {
    public static final Codec<NemosilChunkGenerator> CODEC = RecordCodecBuilder.create(
        (instance) -> {return instance.group(BiomeSource.field_24713.fieldOf("biome_source").forGetter((generator) -> {
            return generator.biomeSource;
        })).apply(instance, instance.stable(NemosilChunkGenerator::new));
    });

    private long seed;
    private Random rand;
    
    private Perlin terrainShape0;
    private Perlin terrainShape1;
    private Perlin terrainInterpolate;
    private Perlin terrainPlainsHeight;
    private Perlin terrainExposedRock;
    private Perlin terrainStrongNoise;
    private Perlin terrainFineNoise;
    /*
    private MapGenBase caveGen;
    private MapGenBase cegliumGen;
    private MapGenBase coalTopGen;
    private MapGenBase coalBotGen;
    private MapGenBase cobbleGen;

     */
    private MusaTreePlainsBiomeGenerator plainsTreeGen;
    private MusaTreeHillsBiomeGenerator hillsTreeGen;

    private boolean init;

    public NemosilChunkGenerator(BiomeSource biomeSource) {
        super(biomeSource, 0);
        init = false;
    }

    public NemosilChunkGenerator(BiomeSource biomeSource, long seed) {
        super(biomeSource, seed);
        
        this.seed=seed;

        /*
        caveGen = new NemosilCaveGenerator();
        cegliumGen = new RealmShiftOreGenerator(10, 50, 2, 2, 4, 7, RealmShiftBlocks.NEMOSIL_CEGLIUM.getDefaultState(), 1);
        coalTopGen = new RealmShiftOreGenerator(70, 200, 1, 20, 3, 6, RealmShiftBlocks.NEMOSIL_COAL.getDefaultState(), 3, RealmShiftBlocks.NEMOSIL_STONE.getDefaultState());
        coalBotGen = new RealmShiftOreGenerator(10, 70, 1, 15, 5, 9, RealmShiftBlocks.NEMOSIL_COAL.getDefaultState(), 8, RealmShiftBlocks.NEMOSIL_STONE.getDefaultState());
        cobbleGen = new RealmShiftOreGenerator(40, 150, 1, 30, 3, 9, RealmShiftBlocks.NEMOSIL_COBBLESTONE.getDefaultState(), 12, RealmShiftBlocks.NEMOSIL_STONE.getDefaultState());
        */
    }

    @Override
    public double getTerrainDensity(int x, int y, int z) {
        RealmShiftBiomeSource biomeProvider = (RealmShiftBiomeSource) this.biomeSource;

        double interpolate = RealmShiftUtil.smoothify((terrainInterpolate.getValue(x, 0, z) / 1.8), 10) * 0.5;
        double density = 0;
        density = terrainShape0.getValue(x, y, z) + (120 - y) / (12.0 + 0 * interpolate * 6.0);
        double plateau_weight = biomeProvider.getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLATEAU, x, z);
        double plains_weight = biomeProvider.getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLAINS, x, z);
        if (plateau_weight > 0) {
            int y_seg = y + (y / 6) * 20;
            // 140+(140/6)*20 = 606;
            double plateau_weight_modulate = MathHelper.clamp(plateau_weight * 4.0, 0, 1);
            density = density * (1 - plateau_weight_modulate)
                    + plateau_weight_modulate * (terrainShape1.getValue(x, y_seg, z) * 0.5 + (606 - y_seg) / 180.0);
        }
        if (plains_weight > 0) {
            double plains_weight_modulate = MathHelper.clamp(plains_weight, 0, 1);
            double fadeSpike = MathHelper.clamp((150.0 - y) / 150.0, 0, 1);
            fadeSpike = 1.0 - 1.0 / (fadeSpike * 30.0 + 1.0);
            double spikes = terrainShape0.getValue(-x * 2.0, y / 20.0, -z * 2.0) * fadeSpike * 500.0 - 400.0;
            spikes = MathHelper.clamp((spikes - 200.0) * 50.0 + 100.0, 0, 1000);
            double plains = terrainShape0.getValue(-z, y, -x) + (90 - y) + terrainPlainsHeight.getValue(x, 0, z) * 15;
            density = density * (1 - plains_weight_modulate) + plains_weight_modulate * (spikes + plains);
        }
        if (this.getRockExposed(x, z)<0.4) {
            double curve = 0.6-this.getRockExposed(x, z);
            curve *= 1-Math.min(Math.max(0, plateau_weight),0.9);
            curve *= 1+Math.min(Math.max(0, plains_weight),1.0)*4.0;
            curve += plains_weight*0.6;
            density += curve;//Math.max(0,Math.min(curve*3.0,1));
        }
        return density;
    }

    public void initNoise(long seed) {
        //if(init)return;
        
        this.rand = new Random();

        if(terrainShape0 == null)terrainShape0 = new Perlin();
        terrainShape0.setSeed((int) seed);
        terrainShape0.setFrequency(0.01);
        terrainShape0.setOctaveCount(4);
        terrainShape0.setLacunarity(2);
        terrainShape0.setPersistence(0.8);

        if(terrainShape1 == null)terrainShape1 = new Perlin();
        terrainShape1.setSeed((int) (seed + 1));
        terrainShape1.setFrequency(0.03);
        terrainShape1.setOctaveCount(2);
        terrainShape1.setLacunarity(2);
        terrainShape1.setPersistence(0.3);

        if(terrainInterpolate == null)terrainInterpolate = new Perlin();
        terrainInterpolate.setSeed((int) (seed + 2));
        terrainInterpolate.setFrequency(0.0001);
        terrainInterpolate.setOctaveCount(1);
        terrainInterpolate.setLacunarity(1);
        terrainInterpolate.setPersistence(1);

        if(terrainPlainsHeight == null)terrainPlainsHeight = new Perlin();
        terrainPlainsHeight.setSeed((int) (seed + 3));
        terrainPlainsHeight.setFrequency(0.004);
        terrainPlainsHeight.setOctaveCount(1);
        terrainPlainsHeight.setLacunarity(2);
        terrainPlainsHeight.setPersistence(1);

        if(terrainExposedRock == null)terrainExposedRock = new Perlin();
        terrainExposedRock.setSeed((int) (seed + 15));
        terrainExposedRock.setFrequency(0.01);
        terrainExposedRock.setOctaveCount(4);
        terrainExposedRock.setLacunarity(1.7);
        terrainExposedRock.setPersistence(0.8);

        if(terrainStrongNoise == null)terrainStrongNoise = new Perlin();
        terrainStrongNoise.setSeed((int) (seed + 6));
        terrainStrongNoise.setFrequency(0.2);
        terrainStrongNoise.setOctaveCount(2);
        terrainStrongNoise.setLacunarity(2);
        terrainStrongNoise.setPersistence(1);
        
        if(terrainFineNoise == null)terrainFineNoise = new Perlin();
        terrainFineNoise.setSeed((int) (seed + 34));
        terrainFineNoise.setFrequency(1);
        terrainFineNoise.setOctaveCount(1);
        terrainFineNoise.setLacunarity(2);
        terrainFineNoise.setPersistence(1);
        
        if(plainsTreeGen == null)plainsTreeGen = new MusaTreePlainsBiomeGenerator();
        if(hillsTreeGen == null)hillsTreeGen = new MusaTreeHillsBiomeGenerator();
        
        init = true;
    }

    @Override
    protected Codec<? extends ChunkGenerator> method_28506() {
        return CODEC;
    }

    @Override
    public ChunkGenerator withSeed(long seed) {
        return new NemosilChunkGenerator(this.biomeSource.withSeed(seed), seed);
    }

    @Override
    public void buildSurface(ChunkRegion region, Chunk chunk) {
        int x = chunk.getPos().x;
        int z = chunk.getPos().z;

        initNoise(region.getSeed());

        this.rand.setSeed(x * 341873128712L + z * 132897987541L);
        
        for (int j = 0; j < 16; ++j){
            for (int k = 0; k < 16; ++k) {
                for (int i = 0; i < 180; ++i) {
                    double density = getTerrainDensity(j + x * 16, i, k + z * 16);
                    // System.out.println(dirt_height);
                    if (density > 0) {
                        chunk.setBlockState(new BlockPos(j, i, k), RealmShiftBlocks.NEMOSIL_STONE.getDefaultState(), false);
                    }
                }
            }
        }
        
        //make stone into grass and dirt
        for (int j = 0; j < 16; ++j){
            for (int k = 0; k < 16; ++k) {
                //make bedrock
                int ni = rand.nextInt(5);
                for(int i=0;i <= ni; ++i) {
                    chunk.setBlockState(new BlockPos(j, i, k), Blocks.BEDROCK.getDefaultState(), false);
                }
                // 0 means air
                // 1 means first block
                // 2 means filler blocks
                // 3 means the rest
                int progress = 0;
                int patient = 30;
                int maxProgress = (int)(getRockExposed(x*16+j,z*16+k)*5.0);
                if(rand.nextInt(10)==0)System.out.println(maxProgress);
                maxProgress = maxProgress<0?0:maxProgress>6?6:maxProgress;
                for (int i = 180; i >= 0; --i) {
                    BlockState state = chunk.getBlockState(new BlockPos(j, i, k));
                    if (state == Blocks.AIR.getDefaultState()) {
                        // start over
                        progress = 0;
                        patient++;
                    } else {
                        if (progress < maxProgress) {
                            if (progress == 0) {
                                chunk.setBlockState(new BlockPos(j, i, k), RealmShiftBlocks.NEMOSIL_GRASS.getDefaultState(), false);
                                placeTallGrassInPlains(chunk, j, i + 1, k, x, z);
                                placeTallGrassInHills(chunk, j, i + 1, k, x, z);
                            } else {
                                if (progress == 1 && rand.nextInt(30) == 0)
                                    progress--;
                                chunk.setBlockState(new BlockPos(j, i, k), RealmShiftBlocks.NEMOSIL_DIRT.getDefaultState(), false);
                            }
                            progress++;
                        } else {
                            if(patient--<0)break;
                        }
                    }
                }
            }
        }
        /*
        caveGen.generate(this.world, x, z, chunk);
        cegliumGen.generate(this.world, x, z, chunk);
        coalTopGen.generate(this.world, x, z, chunk);
        coalBotGen.generate(this.world, x, z, chunk);
        cobbleGen.generate(this.world, x, z, chunk);
        */
    }

    private double getRockExposed(int x, int z) {
        return terrainExposedRock.getValue(x, 0, z)+
               terrainStrongNoise.getValue(x, 0, z)*0.1+
               +0.99;
    }

    @Override
    public void populateNoise(WorldAccess world, StructureAccessor accessor, Chunk chunk) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public int getHeight(int x, int z, Type heightmapType) {
        // TODO Auto-generated method stub
        return 50;
    }

    @Override
    public BlockView getColumnSample(int x, int z) {
        // TODO Auto-generated method stub
        return null;
    }
    
    private void placeTallGrassInPlains(Chunk chunk, int x, int y, int z, int chunk_x, int chunk_z) {
        RealmShiftBiomeSource biomeProvider = (RealmShiftBiomeSource) this.biomeSource;
        //double plateauWeight = biomeProvider.getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLATEAU, x + chunk_x * 16, z + chunk_z * 16);
        double plainsWeight = biomeProvider.getBiomesWeight(RealmShiftBiomes.NEMOSIL_PLAINS, x + chunk_x * 16, z + chunk_z * 16);
        plainsWeight = plainsWeight>1?1:plainsWeight;
        BlockState state = Blocks.AIR.getDefaultState();
        double changeShortTallGrass = this.rand.nextDouble() + 0.3;
        BlockState smallGrass = RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE, NemosilTallGrass.NemosilGrassType.GRASS);
        if (changeShortTallGrass < plainsWeight ||
            changeShortTallGrass < 0.35) {
            state = smallGrass;
            //two block high
            if ((this.rand.nextDouble()) < plainsWeight) {
                state = RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE, NemosilTallGrass.NemosilGrassType.BOTTOM);
                if (chunk.getBlockState(new BlockPos(x, y + 1, z)) == Blocks.AIR.getDefaultState()) {
                    chunk.setBlockState(new BlockPos(x, y + 1, z), RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE, NemosilTallGrass.NemosilGrassType.TOP), false);
                    return;
                }
            }
            //three block high
            if ((this.rand.nextDouble()) < plainsWeight) {
                state = RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE,  NemosilTallGrass.NemosilGrassType.BOTTOM);
                if (chunk.getBlockState(new BlockPos(x, y + 1, z)) == Blocks.AIR.getDefaultState() &&
                    chunk.getBlockState(new BlockPos(x, y + 2, z)) == Blocks.AIR.getDefaultState()) {
                    chunk.setBlockState(new BlockPos(x, y + 2, z), RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE, NemosilTallGrass.NemosilGrassType.TOP), false);
                    chunk.setBlockState(new BlockPos(x, y + 1, z), RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE, NemosilTallGrass.NemosilGrassType.MIDDLE), false);
                    return;
                }
            }
            if (chunk.getBlockState(new BlockPos(x, y, z)) == smallGrass) {
                if (this.rand.nextDouble() < 0.002) {
                    state = RealmShiftBlocks.NEMOSIL_FEVERDEW.getDefaultState();
                }
                if (this.rand.nextDouble() < 0.02 && y > 120) {
                    state = RealmShiftBlocks.NEMOSIL_VIPER_BERRY.getDefaultState();
                }
            }
        }
        chunk.setBlockState(new BlockPos(x, y, z), state, false);
    }

    private void placeTallGrassInHills(Chunk chunk, int x, int y, int z, int chunk_x, int chunk_z) {
        NemosilBiomeSource biomeProvider = (NemosilBiomeSource) this.biomeSource;
        double weight = biomeProvider.getTreesWeight(RealmShiftBiomes.NEMOSIL_HILLS, x + chunk_x * 16, z + chunk_z * 16);

        if (weight > 0) {
            if (this.rand.nextDouble() < weight + 0.05) {
                chunk.setBlockState(new BlockPos(x, y, z), RealmShiftBlocks.NEMOSIL_TALL_GRASS.getDefaultState().with(NemosilTallGrass.GRASS_TYPE, NemosilTallGrass.NemosilGrassType.GRASS), false);
            }
        }
    }

    @Override
    public void generateFeatures(ChunkRegion region, StructureAccessor accessor) {
        int x = region.getCenterChunkX();
        int z = region.getCenterChunkZ();
        System.out.println(x + " , " + z);
        //boolean villageGenerated = false;
        BlockPos blockpos = new BlockPos(x*16, 0, z*16).add(8,0,8);
        this.rand.setSeed(region.getSeed());
        long k = this.rand.nextLong() / 2L * 2L + 1L;
        long l = this.rand.nextLong() / 2L * 2L + 1L;
        this.rand.setSeed(x * k ^ region.getSeed() + z * l ^ region.getSeed());
        //region.setBlockState(blockpos.add(0, 200, 0), RealmShiftBlocks.FORGED_END_STONE.getDefaultState(), 1);
        this.plainsTreeGen.generate(region.getWorld(), this.rand, blockpos);
        this.hillsTreeGen.generate(region.getWorld(), this.rand, blockpos);
    }


    public int getGroundHeight() {
        return 130;
    }
}
