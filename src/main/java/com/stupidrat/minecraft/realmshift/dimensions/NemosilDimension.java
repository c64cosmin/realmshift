/*
package com.stupidrat.minecraft.realmshift.dimensions;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.chunk.ChunkGenerator;

public class NemosilDimension extends RealmShiftDimension {

	public NemosilDimension(World world, DimensionType type, float f) {
		super(world, type, f);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ChunkGenerator<?> createChunkGenerator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public BlockPos getSpawningBlockInChunk(ChunkPos chunkPos, boolean checkMobSpawnValidity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public BlockPos getTopSpawningBlockPosition(int x, int z, boolean checkMobSpawnValidity) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public float getSkyAngle(long timeOfDay, float tickDelta) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public boolean hasVisibleSky() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Vec3d getFogColor(float skyAngle, float tickDelta) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean canPlayersSleep() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean isFogThick(int x, int z) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public DimensionType getType() {
		// TODO Auto-generated method stub
		return null;
	}

    /*private NemosilBiomeProvider biomeProvider;

    public NemosilDimension(World worldIn, DimensionType typeIn) {
        super(worldIn, typeIn);

        this.biomeProvider = new NemosilBiomeProvider(this.getSeed());
        if(this.world.isRemote){
            RendererNemosilSky render = new RendererNemosilSky();
            setSkyRenderer(render);
        }
    }

    //generates the chunks
    @Override
    public ChunkGenerator<?> createChunkGenerator()
    {
        return new NemosilChunkGenerator(this.world, new NemosilBiomeProvider(this.world.getSeed()), null);
    }

    @Override
    public float calculateCelestialAngle(long worldTime, float partialTicks)
    {
        int i = (int)(worldTime % 36000L);
        float f = (i + partialTicks) / 36000.0F - 0.25F;

        if (f < 0.0F)
        {
            ++f;
        }

        if (f > 1.0F)
        {
            --f;
        }

        return f;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public Vec3d getSkyColor(BlockPos cameraPos, float partialTicks)
    {
        float sky = this.calculateCelestialAngle(this.world.getGameTime(), partialTicks)*(float)Math.PI*2;
        double skyInt = MathHelper.clamp(MathHelper.cos(sky), 0f, 1f);
        return new Vec3d(skyInt*0.8,skyInt*0.4,0);
    }

    @Nullable
    @Override
    @OnlyIn(Dist.CLIENT)
    public float[] calcSunriseSunsetColors(float celestialAngle, float partialTicks){
        return null;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public Vec3d getFogColor(float p_76562_1_, float p_76562_2_)
    {
        return new Vec3d(0,0,0);
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public boolean isSkyColored()
    {
        return true;
    }

    @Override
    public boolean canRespawnHere()
    {
        return true;
    }

    @Override
    public boolean isSurfaceWorld()
    {
        return false;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public float getCloudHeight()
    {
        return 63.0F;
    }

    @Override
    public boolean canCoordinateBeSpawn(int x, int z)
    {
        return false;
    }

    /*@Override
    public int getAverageGroundLevel()
    {
        return 120;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public double getVoidFogYFactor()
    {
        return 0.06;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public boolean doesXZShowFog(int x, int z)
    {
        return false;
    }

    @Override
    public int getHeight()
    {
        return 256;
    }

    @Override
    public int getActualHeight()
    {
        return 256;
    }

    @Override
    public float getSunBrightness(float partialTicks)
    {
        float f = this.calculateCelestialAngle(this.world.getGameTime(), partialTicks);
        float f1 = MathHelper.cos(f * ((float)Math.PI * 2F))*3.0F+1.0F;
        f1 = MathHelper.clamp(f1, 0.0F, 1.0F);
        return f1;
    }

    @Override
    public boolean isDaytime(){
        return getSunBrightness(0)>0.2;
    }

    @Override
    public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
        Random rnd = new Random(this.getSeed() + chunkPosIn.x + chunkPosIn.z);
        return this.findSpawn(chunkPosIn.x << 4 + rnd.nextInt(16), chunkPosIn.z << 4 + rnd.nextInt(16), checkValid);
    }

    @Override
    public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
        BlockPos spawn = new BlockPos(posX, this.getHeight(), posZ);
        while(!this.world.getBlockState(spawn).getMaterial().isSolid() &&
              !this.world.getBlockState(spawn).getMaterial().isOpaque())
            spawn.down();
        return spawn;
    }
    
    @Override
    public Biome getBiome(BlockPos pos) {
        return RealmShiftBiomes.NEMOSIL_HILLS;
    }
    
}
*/