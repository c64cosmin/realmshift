package com.stupidrat.minecraft.realmshift.dimensions;

import com.stupidrat.minecraft.realmshift.RealmShiftMod;
import com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil.NemosilChunkGenerator;

import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;

public class RealmShiftDimensions {
    public static final RegistryKey<World> NEMOSIL = RegistryKey.of(Registry.DIMENSION, new Identifier(RealmShiftMod.MODID, "nemosil"));

    public static void register() {
        Registry.register(Registry.CHUNK_GENERATOR, new Identifier(RealmShiftMod.MODID, "nemosil"), NemosilChunkGenerator.CODEC);
    }
/*
        ModDimension nemosil = new ModDimension() { @Override public BiFunction<World, DimensionType, ? extends Dimension> getFactory() {return NemosilDimension::new;}};
        ResourceLocation nemosil_name = new ResourceLocation(RealmShiftMod.MODID, "realmshift_nemosil");
        nemosil.setRegistryName(nemosil_name);
        RealmShiftDimensions.NEMOSIL = DimensionManager.registerDimension(nemosil_name, nemosil, null, false);
    }
    */
    //public static final DimensionType NEMOSIL = DimensionType.register("overworld", new DimensionType(5, "", "realmshift_nemosil", NemosilDimension::new, false));
}
