package com.stupidrat.minecraft.realmshift.dimensions.generators.nemosil;

import java.util.Random;

import com.stupidrat.minecraft.realmshift.blocks.RealmShiftBlocks;
import com.stupidrat.minecraft.realmshift.blocks.RealmShiftSapling;
import com.stupidrat.minecraft.realmshift.dimensions.generators.IFeatureGenerator;

import net.minecraft.block.BlockState;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ChunkRegion;

public class MusaTreePlainsBiomeGenerator implements IFeatureGenerator{
    public MusaTreePlainsBiomeGenerator(){
    }

    @Override
    public void generate(ServerWorld world, Random rand, BlockPos pos){
        //double weight = ((NemosilBiomeSource)biomeSource).getTreesWeight(RealmShiftBiomes.NEMOSIL_HILLS, pos.getX(), pos.getZ());

        int n = 1;
        BlockPos treeBases[] = new BlockPos[n];

        for(int i=0;i<n;i++){
            int offX = rand.nextInt(16)-2;
            int offY = rand.nextInt(16)-2;
            BlockPos top = pos.add(0, 150, 0);
            /*while(!world.getBlockState(top).getMaterial().isSolid()) {
                top.down();
            }*/
            treeBases[i] = top;
        }
        for(BlockPos root : treeBases)
        if(rand.nextDouble() < 0.05) {//*0.01){
            //world.setBlockState(root, RealmShiftBlocks.NEMOSIL_DIRT.getDefaultState());
            //((RealmShiftSapling)RealmShiftBlocks.NEMOSIL_MUSA_SAPLING).generator.generate(world, rand, root);
        }
    }

    @Override
    public void generate(ServerWorld world, Random rand, BlockPos pos, BlockState state) {
        this.generate(world, rand, pos);
    }
}